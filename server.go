package main

import (
	"fmt"
	"log"
	"net/http"
	"runtime"
	"time"

	"github.com/fuber/util" // HL
)

// RootHandler handles the main API requests
func RootHandler(w http.ResponseWriter, req *http.Request) {
	hj, ok := w.(http.Hijacker)
	if !ok {
		http.Error(w, "webserver doesn't support hijacking", http.StatusInternalServerError)
		return
	}
	conn, bufrw, err := hj.Hijack()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer conn.Close()
	bufrw.WriteString(util.ChunkedHeader())
	for _ = range time.NewTicker(2 * time.Second).C {
		bufrw.WriteString("5\r\n")
		// Where the magic happens
		bufrw.WriteString("💩 \r\n")
		bufrw.Flush()
	}
}

func main() {
	// more procs = more win
	runtime.GOMAXPROCS(8)
	http.HandleFunc("/", RootHandler)
	http.HandleFunc("/marketing", MarketingHandler)
	http.HandleFunc("/served", ServedHandler)
	http.HandleFunc("/legal", ISOHandler)
	http.HandleFunc("/taxis", TaxiHandler)
	port := 12345
	// Startup message
	fmt.Printf("Running Fuber API server at :%d\n", port)
	err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
