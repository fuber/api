package main

import "net/http"

var iso string

// ISOHandler returns date of binary compilation
// /legal
func ISOHandler(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte(iso + "\n"))
}
