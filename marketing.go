package main

import (
	"fmt"
	"net/http"
)

var marketingCopy string = "Use Fuber!\n"

// served is the number of engagements
var served int = 0

// MarketingHandler returns marketing stuff
// /marketing
func MarketingHandler(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte(marketingCopy))
	served += 1
}

// ServedHandler returns number of marketing requests served
// /served
func ServedHandler(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte(fmt.Sprintf("%d\n", served)))
}
