//go:generate ruby data/taxi_gen.rb data/fubers.csv taxis_list.go
//go:generate go fmt taxis_list.go

package main

import (
	"encoding/json"
	"net/http"
)

type Taxi struct {
	Name  string
	Price int
}

// non initialized
var Taxis []Taxi // HL

// TaxiHandler returns taxis
func TaxiHandler(w http.ResponseWriter, req *http.Request) {
	if data, err := json.MarshalIndent(Taxis, " ", "  "); err == nil {
		w.Write(data)
	}
}
